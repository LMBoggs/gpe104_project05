﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowPawn : Pawn {

    //Inherited from pawn
    /*
    public int health; // designer friendly var to store pawn health
    [HideInInspector] public int healthStart; // stores original value of health at game start
    public int atkDmg; // designer friendly var to store attack damage
    public float speed; //designer friendly var to store speed
    */

    //Declare variables
    public float lifetime; // designer frinedly var to store lifetime of arrow
    private Vector3 playerPosition; // stores location of player on start of this object
    private Vector3 direction; //result of direction between arrow and player
    private float angle; // stores angle to player on start
    private bool isDealt = false; //stores if the arrow has already done its damage

    // Use this for initialization
    public override void Start () {

        //Load variables
        playerPosition = GameObject.FindWithTag("Player").transform.position;

        //call start function of Pawn parent
        base.Start();

        //Destroy this game object after designer set time
        Destroy(this.gameObject, lifetime);

        //Find direction to player on start
        direction = playerPosition - transform.position;
        direction.Normalize();


        //Rotate to face player on start
        angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg; //calculate angle based on direction to player
        transform.rotation = Quaternion.Euler(0, 0, angle); //update angle to rotate towards



    }
	
	// Update is called once per frame
	void Update () {

        //Move towards player position on start
        //--------------------------------------
        transform.position += direction * speed;



    }

    //Arrow hits an object
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Check for player tag
        if (other.gameObject.CompareTag("Player"))
        {
            //If arrow has not dealt damage already
            if (!isDealt)
            {
                //Player takes damage
                other.gameObject.GetComponent<Pawn>().TakeDamage(atkDmg);

                //update isDealt
                isDealt = true;
            }

            

        }

        //Destroy the arrow
        Destroy(this.gameObject);
    }


    public override void TakeDamage(int dmg)
    {
        //Arrows are destroyed if taking damage
        Destroy(this.gameObject);
    }


}
