﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {

    //Declare variables
    public int health = 1; // designer friendly var to store pawn health, initialized to 1
    [HideInInspector] public int healthStart; // stores original value of health at game start
    public int atkDmg = 1; // designer friendly var to store attack damage
    public float speed = 1; //designer friendly var to store speed

	// Use this for initialization
	public virtual void Start () {

        //store original health value on start
        healthStart = health; 

	}
	
    public virtual void Idle()
    {

    }


    public virtual void Move(bool jump, bool attack)
    {

    }

    public virtual void Attack()
    {

    }

    public virtual void TakeDamage(int dmg)
    {
        health -= dmg;
        Debug.Log("TakeDamage() called from parent class");
    }

}
