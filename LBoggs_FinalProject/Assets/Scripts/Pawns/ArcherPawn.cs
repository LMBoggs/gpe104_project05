﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherPawn : Pawn {

    //Inherited from pawn
    /*
    public int health; // designer friendly var to store pawn health
    [HideInInspector] public int healthStart; // stores original value of health at game start
    public int atkDmg; // designer friendly var to store attack damage
    public float speed; //designer friendly var to store speed
    */

    //Declare variables
    private Animator myAnim; // var to hold Archer animator
    public GameObject arrowPrefab; // var to hold arrow prefab


    // Use this for initialization
    public override void Start () {

        //call start function of parent class
        base.Start();

        //load my animator to variable
        myAnim = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Idle()
    {
        myAnim.SetBool("isAttacking", false);
    }

    public override void TakeDamage(int dmg)
    {
        health -= dmg; 

        //If all health is lost
        if (health <= 0)
        {
            myAnim.Play("Archer_Death");
        }
    }

    public override void Attack()
    {
        //Create arrow and animate archer
        //---------------------     
        //Create arrow at arrow position and rotation
        GameObject arrow = Instantiate(arrowPrefab, transform.Find("ArrowPosition").position, transform.Find("ArrowPosition").rotation);
        myAnim.SetBool("isAttacking", true);

        //Give arrow the attack damage of archerPawn
        arrow.GetComponent<ArrowPawn>().atkDmg = atkDmg;
    }


    //When reset by lap
    private void OnEnable()
    {
        //Set health back to start health
        health = healthStart;
    }
}
