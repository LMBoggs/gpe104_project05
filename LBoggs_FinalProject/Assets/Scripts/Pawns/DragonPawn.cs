﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonPawn : Pawn {

    //Inherited variables from Pawn
    //------------------------------------------------------------------
    //public int health; // designer friendly var to store pawn health
    //[HideInInspector] public int healthStart; // stores original value of health at game start
    //public int atkDmg; // designer friendly var to store attack damage
    //public float speed; //designer friendly var to store speed
    //------------------------------------------------------------------

    //Declare variables
    private Rigidbody2D rb2d; //the rididbody 2d of this pawn
    public float jumpForce; // designer friendly float to store strength of dragon jump
    public int maxJumps = 2; // des. friendly var to store max jumps
    private int jumpCount = 0; // counts current number of jumps player has taken before landing
    private Animator myAnim; // variable to store animator on this object
    public GameObject firePrefab; // designer friendly variable to store fireball prefab

    // Use this for initialization
    public override void Start () {

        //call base start
        base.Start();

        //set variables
        rb2d = GetComponent<Rigidbody2D>();
        myAnim = GetComponent<Animator>();

        //If pawn is player pawn, use gameManager variables instead
        if (CompareTag("Player"))
        {
            health = GameManager.instance.MAX_HEALTH;
            healthStart = health; 
        }

	}
	
	// Update is called once per frame
	void Update () {
		


	}

    public override void Move(bool jump, bool attack)
    {
        //Constantly move dragon pawn to the right
        transform.position = transform.position + (Vector3.right * speed * Time.deltaTime);
        //rb2d.AddForce(transform.right * speed);


        //if jump is true
        if (jump)
        {
            //if jumpCount is not met with maxJumps
            if (jumpCount < maxJumps)
            {
                //Apply jump force
                rb2d.AddForce(transform.up * jumpForce);

                //Animate, set bool isGrounded to false in animator fsm
                myAnim.SetBool("isGrounded", false);

                //increment jumpCount
                jumpCount++;

               

            }
            
            
        }

        //if attack is true, call attack function
        if (attack)
        {
            Attack();

        }

    }

    //Collision for grounding
    private void OnCollisionEnter2D(Collision2D other)
    {
        //Check for Platform tag
        if (other.gameObject.CompareTag("Platform"))
        {
            //Set isGrounded bool in Animator FSM
            myAnim.SetBool("isGrounded", true);

            //Reset jumpCount
            jumpCount = 0;
        }
    }

    //Collision for falling
    private void OnCollisionExit2D(Collision2D other)
    {
        //Check for Platform tag
        if (other.gameObject.CompareTag("Platform"))
        {
            //Set isGrounded bool in Animator FSM
           // myAnim.SetBool("isGrounded", false);
        }
    }


    public override void Attack()
    {
        //Create Fireball and animate dragon
        //---------------------     
        //Create bullet at fire position and rotation
        GameObject fireball = Instantiate(firePrefab, transform.Find("firePosition").position, transform.Find("firePosition").rotation);
        myAnim.SetBool("isAttacking", true);
        

        //Give fireball the attack damage of dragon pawn
        fireball.GetComponent<Fireball>().damage = atkDmg;
    }


    public override void TakeDamage(int dmg)
    {
        //Deduct health as player pawn
        if (CompareTag("Player"))
        {
            GameManager.instance.playerHealth--;
        }

        //As normal pawn
        health--;

        //Update Health Bar

        //If health falls to 0, die
        if (health <= 0)
        {
            //Set integer in animator fsm
            myAnim.SetInteger("Health", 0);
        }

        

    }




}
