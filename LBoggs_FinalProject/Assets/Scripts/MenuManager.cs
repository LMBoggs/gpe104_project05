﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    
    //Play Level
    public void PlayLevel()
    {
        SceneManager.LoadScene("Level_01");
    }

    //Return to MainMenu
    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    
    //Quit the game
    public void QuitGame()
    {
        Application.Quit();
    }


}
