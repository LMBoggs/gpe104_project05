﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallZone : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Check player tag
        if (other.gameObject.CompareTag("Player"))
        {
            //Set player health to zero
            //Note: Right now, this calls GameOver screen in GameManager
            GameManager.instance.playerHealth = 0;
        }
    }


}
