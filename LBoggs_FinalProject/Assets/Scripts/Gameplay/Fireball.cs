﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour {

    //Declare variables
    public float speed = 2; // designer friendly float to controll speed of fireball
    public float lifetime = 2.0f; // designer friendly float to control how long the fireball exists in the game
    [HideInInspector] public int damage = 1; // value to store damage of this fireball, effected by player attack damage on created

	// Use this for initialization
	void Start () {

        //Destroy the fireball after designer set time
        Destroy(this.gameObject, lifetime);

	}
	
	// Update is called once per frame
	void Update () {

        //Make the fireball fly to the right at a designer set speed
        transform.position = transform.position + (Vector3.right * speed * Time.deltaTime);


	}


    //Hitting objects and enemies
    private void OnTriggerEnter2D(Collider2D other)
    {
            
        //Check for enemy tag
        if (other.gameObject.CompareTag("Enemy"))
        {
            //Call take damage function on enemy pawn
            other.gameObject.GetComponent<Pawn>().TakeDamage(damage);
            
        }

        //Destroy the fireball
        Destroy(this.gameObject);

    }


}
