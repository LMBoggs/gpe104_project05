﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour {

    //Declare variables
    public int damage;
    private bool isTriggered = false; //stores if the player has already hit this spike on this round
    

    //When an object enters the trigger area
    private void OnTriggerEnter2D(Collider2D other)
    {
        //If that object was the player
        if (other.gameObject.CompareTag("Player"))
        {
            //If player hasn't triggered this trap yet, deal damage
            if (!isTriggered)
            {
                //Player takes damage
                other.gameObject.GetComponent<Pawn>().TakeDamage(damage);
               

                //set trap to triggered
                isTriggered = true;
            }
            
        }
       
    }

    
}
