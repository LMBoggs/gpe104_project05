﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    //Declare variables
    private Image healthSprite; //variable to hold our timer sprite
    public float healthPercent; // variable to hold percentage of timer to draw
    public Color fullColor;
    public Color twoThirdsColor;
    public Color oneThirdColor;

    // Use this for initialization
    void Start () {

        //Set variables
        healthSprite = GetComponent<Image>(); //get the image from this object

        // Make sure the image component is set to filled
        healthSprite.type = Image.Type.Filled;


        //Set to full on start
        healthSprite.fillAmount = 1;

        //Set percent on start
        healthPercent = GameManager.instance.playerHealth / GameManager.instance.MAX_HEALTH;


    }
	
	// Update is called once per frame
	void Update () {

        //update health percentage
        healthPercent = (float)GameManager.instance.playerHealth / GameManager.instance.MAX_HEALTH;

        //Display perecentage of bar
        healthSprite.fillAmount = healthPercent;


        //SET COLORS
        //-----------------------------------------
        // 3 thirds
        if (healthPercent >= 0.66)
        {
            //Set to full color
            healthSprite.color = fullColor;
        }

        // 2 thirds
        else if (healthPercent < 0.66 && healthPercent >= 0.33)
        {
            //Set to full color
            healthSprite.color = twoThirdsColor;
        }

        // 1 third
        else if (healthPercent < 0.33 && healthPercent >= 0.0)
        {
            //Set to full color
            healthSprite.color = oneThirdColor;
        }

        




    }
}
