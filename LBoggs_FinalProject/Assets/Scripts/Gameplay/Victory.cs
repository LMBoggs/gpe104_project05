﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Victory : MonoBehaviour {

    public float victoryTimer = 5.0f; // des. friendly var to store how long to wait until loading victory screen
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Check for player tag
        if (other.gameObject.CompareTag("Player"))
        {
            //Find Game Controller and disable it
            GameObject.FindWithTag("GameController").SetActive(false);

            //play fly animation directly on dragon pawn
            other.gameObject.GetComponent<Animator>().Play("Red_Fly");

            Invoke("Invoke_Victory", victoryTimer);
        }

        


    }

    private void Invoke_Victory()
    {
        SceneManager.LoadScene("Victory");
    }

}
