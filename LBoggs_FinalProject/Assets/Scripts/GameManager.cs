﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //DECLARE VARIABLES
    //-------------------------------------------------------------------------

    //Player Pawn
    //[HideInInspector]
    public Pawn playerPawn; //hidden from inspector, not for designer manipulation

    //Health
    public int MAX_HEALTH; //Des. friendly var to store player's max health
    [HideInInspector] public int playerHealth; // var to use throughout code

    //Lives
    public int START_LIVES; // Des. friendly var to store how many lives the player starts with
    [HideInInspector] public int playerLives;  //player lives to use throughout code

    //Orbs
    public int orbsforLife; //Des. friendly var to determine how many spirit orbs will gain the player a life
    [HideInInspector] public int playerOrbs; // player orbs to use throughout code

    //Score
    [HideInInspector] public int score; 

    //Singleton variable
    [HideInInspector] public static GameManager instance;

    //--------------------------------------------------------------------------

    //FUNCTIONS
    //--------------------------------------------------------------------------

    //Awake
    private void Awake()
    {
        //If no GameManager exists
        if (instance == null)
        {
            //Load this GameManager to singleton
            instance = this; 
        }

        //If we already have a GameManager
        else
        {
            //destroy this extra GameManager
            Destroy(this.gameObject);
            Debug.Log("A second GameManager was detected and destroyed.");
        }
    }

    //Start [Sets variables]
    void Start () {

        //set variables
        playerLives = START_LIVES; //player starts with starting number of lives
        playerHealth = MAX_HEALTH; // player starts with max health
        playerOrbs = 0; //player has not collected any orbs on start
        score = 0; // start with a score of 0
        playerPawn = GameObject.FindWithTag("Player").GetComponent<Pawn>(); //find the player pawn in scene and load it to var


    }
	
	//Update [Called once per frame]
	void Update () {
		
        //If player health reaches 0, load gameOver
        if (playerHealth <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }

	}

    //Reset [Resets the game on playAgain]
    void Reset()
    {

    }
}
