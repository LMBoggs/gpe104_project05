﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller {

    //public Pawn variable called "pawn" is inherited from Controller class

    // Use this for initialization
    void Start () {

        //Load my pawn to variable
        pawn = GetComponentInParent<Pawn>();


	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Trigger enter
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Check Player tag
        if (other.gameObject.CompareTag("Player"))
        {
            //Attack the player
            pawn.Attack();
        }
    }

    //Trigger exit
    private void OnTriggerExit2D(Collider2D other)
    {
        //Check Player tag
        if (other.gameObject.CompareTag("Player"))
        {
            //Resume Idle
            pawn.Idle();
        }
    }

}
