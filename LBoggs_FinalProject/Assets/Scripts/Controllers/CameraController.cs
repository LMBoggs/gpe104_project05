﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : Controller {

    //inherits Pawn pawn from Controller
    private Transform pawnTF; // variable to store transform component of pawn
    public float offsetX; // Designer friendly var to change x camera offset
    public float offsetY; //Designer friendly var to change y camera offset

    // Use this for initialization
    void Start () {

        //Load pawn with player pawn on start
        pawn = GameObject.FindWithTag("Player").GetComponent<Pawn>();

        //Load transform of pawn to variable
        pawnTF = pawn.transform; 
    }

    //Once per frame
    void LateUpdate()
    {

        //Set camera position to player pawn x position plus offset, maintain y position (do not jump with pawn)
        transform.position = new Vector3(pawnTF.position.x + offsetX, pawnTF.position.y + offsetY, transform.position.z);
    }



}
