﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

    //Declare variables

    //public Pawn variable called "pawn" is inherited from Controller class
    private bool jumpPressed; //stores true if the player is pressing the jump button
    private bool attackPressed; // stores true if the player has pressed the attack button



	// Use this for initialization
	void Start () {

        //Set variables
        //load the player pawn in the scene on start
        pawn = GameObject.FindWithTag("Player").GetComponent<Pawn>();

        //Player begins with jump and attack set to false
        jumpPressed = false;
        attackPressed = false; 
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //update jump bool if input received
        if (Input.GetButtonDown("Jump"))
        {
            jumpPressed = true;
        }

        //update attack bool if input received
        if (Input.GetButtonDown("Fire"))
        {
            attackPressed = true;
        }

        //Call Pawn move function every frame
        pawn.Move(jumpPressed, attackPressed);

        //reset bools after calling movement function
        jumpPressed = false;
        attackPressed = false;
		
	}




}
